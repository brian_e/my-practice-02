module.exports = {
  entry: './app/index.js',
  output: {filename: './dist/app/index.js'},

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        loader: 'babel-loader',
        options: {
          presets: ['es2015', 'react']
        }
      }
    ]
  }

};
