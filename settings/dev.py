SECRET_KEY = 'a'
DEBUG = True

INSTALLED_APPS = [
    'django.contrib.staticfiles',
    'app'
]

ROOT_URLCONF = 'urls'
MIDDLEWARE = []

TEMPLATES = [
    {
        'APP_DIRS': True,
        'BACKEND': 'django.template.backends.django.DjangoTemplates'
    }
]

STATIC_URL = '/static/'
STATICFILES_DIRS = ['./dist']
