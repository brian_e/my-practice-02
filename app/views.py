import django.views.generic

class Index(django.views.generic.TemplateView):
    template_name = 'app/index.html'
