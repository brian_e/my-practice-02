import django.conf.urls
import app.views

urlpatterns = [
    django.conf.urls.url(r'^$', app.views.Index.as_view())
]
